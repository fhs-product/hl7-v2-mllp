package com.hapi.listener.demo;

import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.ConnectionListener;
import ca.uhn.hl7v2.app.HL7Service;
import ca.uhn.hl7v2.protocol.ApplicationRouter;
import ca.uhn.hl7v2.protocol.ReceivingApplicationExceptionHandler;
import com.hapi.listener.demo.hl7.server.DemoReceiverApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ca.uhn.hl7v2.validation.impl.ValidationContextFactory;

import javax.annotation.PostConstruct;
import java.util.Map;

@SpringBootApplication
public class HL7ListenerDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(HL7ListenerDemoApplication.class, args);
    }

    @Autowired
    HapiContext hapiContext;
    @Autowired
    DemoReceiverApplication demoReceiverApplication;

    @PostConstruct
    private void postConstruct() throws InterruptedException {
        startHL7Listener();
    }

    private void startHL7Listener() throws InterruptedException {
        //Novalidation if needed
        hapiContext.setValidationContext(ValidationContextFactory.noValidation());
        HL7Service server = hapiContext.newServer(33333, false);
        server.registerApplication("*", "*", demoReceiverApplication);
        server.registerConnectionListener(new MyConnectionListener());
        server.setExceptionHandler(new MyExceptionHandler());

        // Start the server listening for messages
        server.startAndWait();
        System.out.println("HL7 listener is ready");
    }

    public static class MyConnectionListener implements ConnectionListener {
        public void connectionReceived(Connection theC) {
            System.out.println("New connection received: " + theC.getRemoteAddress().toString());
        }

        public void connectionDiscarded(Connection theC) {
            System.out.println("Lost connection from: " + theC.getRemoteAddress().toString());
        }

    }

    public class MyExceptionHandler implements ReceivingApplicationExceptionHandler {
        public String processException(String theIncomingMessage, Map<String, Object> theIncomingMetadata, String theOutgoingMessage, Exception theE) {
            if(theOutgoingMessage == null) {

                System.out.println("processException: theOutgoingMessage is null");
            } else {
                System.out.println("processException: " + theOutgoingMessage.replaceAll("\\r", "\n"));
            }

            System.out.println(" theIncomingMessage: " + theIncomingMessage);
            System.out.println(" theOutgoingMessage: " +  theOutgoingMessage);
            System.out.println(" theIncomingMetadata 1: " +  String.valueOf(theIncomingMetadata.get(ApplicationRouter.METADATA_KEY_SENDING_IP)));
            System.out.println(" theIncomingMetadata 2: " + String.valueOf(theIncomingMetadata.get(ApplicationRouter.METADATA_KEY_SENDING_PORT)));

            return theOutgoingMessage;
        }

    }

}
