package com.hapi.listener.demo.hl7.server;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v251.datatype.HD;
import ca.uhn.hl7v2.model.v251.datatype.MSG;
import ca.uhn.hl7v2.model.v251.datatype.XPN;
import ca.uhn.hl7v2.model.v251.message.ADT_A01;
import ca.uhn.hl7v2.model.v251.message.ADT_A02;
import ca.uhn.hl7v2.model.v251.segment.MSH;
import ca.uhn.hl7v2.model.v251.segment.PID;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import ca.uhn.hl7v2.util.Terser;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class DemoReceiverApplication implements ReceivingApplication<Message> {

    @Autowired
    HapiContext hapiContext;

    public boolean canProcess(Message theIn) {
        return true;
    }

    public Message processMessage(Message theMessage, Map<String, Object> theMetadata) throws HL7Exception {
        Parser p = hapiContext.getGenericParser();

        String encodedMessage = hapiContext.getPipeParser().encode(theMessage);

        Message hapiMsg;
        try {
            // The parse method performs the actual parsing
            hapiMsg = p.parse(encodedMessage);

            if (hapiMsg instanceof ADT_A01) {
                processAdtA01((ADT_A01) hapiMsg);
            } else if (hapiMsg instanceof ADT_A02) {
                processAdtA02((ADT_A02) hapiMsg);
            } // etc...

            Message ackMessage = theMessage.generateACK();
            System.out.println("ACK Message :\n" + hapiContext.getPipeParser().encode(ackMessage).replaceAll("\\r", "\n") + "\n\n");

            return ackMessage;
        } catch (Exception e) {
            e.printStackTrace();
            throw new HL7Exception("");
        }
    }

    /**
     * https://hapifhir.github.io/hapi-hl7v2/xref/ca/uhn/hl7v2/examples/ExampleParseMessages.html
     *
     * @param hapiMsg
     * @throws HL7Exception
     */
    private void processAdtA01(ADT_A01 hapiMsg) throws HL7Exception {
        System.out.println("\n>>> processing ADT_A01");
        Terser terser = new Terser(hapiMsg);

        String messageCode = terser.get("/.MSH-9-1");
        System.out.println("- messageCode: "+messageCode);
        String triggerEvent = terser.get("/.MSH-9-2");
        System.out.println("- triggerEvent: "+triggerEvent);

        String familyName = terser.get("/.PID-5-1");
        System.out.println("- familyName: "+familyName);

        String insuranceCompanyName = terser.get("/.IN1-4");
        System.out.println("- insuranceCompanyName: "+insuranceCompanyName);

        System.out.println();
    }

    /**
     * https://hapifhir.github.io/hapi-hl7v2/xref/ca/uhn/hl7v2/examples/ExampleUseTerser.html
     *
     * @param hapiMsg
     * @throws HL7Exception
     */
    private void processAdtA02(ADT_A02 hapiMsg) throws HL7Exception {
        System.out.println("\n>>> processing ADT_A02");

        MSH messageHeader = hapiMsg.getMSH();
        MSG messageType = messageHeader.getMessageType();

        String messageCode = messageType.getMsg1_MessageCode().toString();
        System.out.println("- messageCode: "+messageCode);
        String triggerEvent = messageType.getMsg2_TriggerEvent().toString();
        System.out.println("- triggerEvent: "+triggerEvent);

        PID patienIdentification = hapiMsg.getPID();
        XPN patientName = patienIdentification.getPatientName(0);
        System.out.println("- familyName: "+patientName.getFamilyName().getFn1_Surname().getValue());

        System.out.println();
    }

}